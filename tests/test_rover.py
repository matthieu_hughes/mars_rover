"""Tests for Rover class."""

import pytest

from mars_rover.rover import Rover, BEARING_TO_COMPASS_DIRECTION


@pytest.mark.parametrize(
    "x,y,max_x,max_y",
    [
        (4, 0, 3, 3),
        (0, 4, 3, 3),
        (1, 1, 0, 0),
        (-1, 0, 2, 2),
        (0, -2, 2, 2),
    ]
)
def test_rover_position_validation(x, y, max_x, max_y):
    """Test validation when initializing the rover's position."""
    with pytest.raises(ValueError):
        Rover(
            x=x,
            y=y,
            compass_bearing='N',
            max_x=max_x,
            max_y=max_y,
            name='test'
        )


@pytest.mark.parametrize(
    "bearing",
    ["A", "SW", 90, '', None]
)
def test_rover_bearing_validation(bearing):
    """Test validation when initializing the rover's bearing."""
    with pytest.raises(ValueError):
        Rover(
            x=0,
            y=0,
            compass_bearing=bearing,
            max_x=2,
            max_y=2,
            name='test'
        )


@pytest.mark.parametrize(
    "x,y,bearing,is_at_edge",
    [
        # Test with 3x3 grid
        (0, 1, 'W', True),
        (3, 2, 'E', True),
        (1, 0, 'S', True),
        (2, 3, 'N', True),
        (2, 2, 'N', False),
        (1, 2, 'W', False),
        (0, 1, 'N', False),
        (0, 1, 'E', False),
        (2, 0, 'N', False),
    ]
)
def test_rover_is_at_edge_of_grid(x, y, bearing, is_at_edge):
    """Test the is_at_edge_of_grid property."""
    rover = Rover(
        x=x,
        y=y,
        compass_bearing=bearing,
        max_x=3,
        max_y=3,
        name='test'
    )
    assert rover.is_at_edge_of_grid == is_at_edge


def test_rover_move_off_edge_of_grid():
    """Test a ValueError is raised when trying to move past the edge of the
    grid."""
    rover = Rover(
        x=3,
        y=0,
        compass_bearing='E',
        max_x=3,
        max_y=3,
        name='test',
    )
    with pytest.raises(ValueError):
        rover.process_instruction('M')


@pytest.mark.parametrize(
    "instruction",
    ["A", "S", 1, '', None]
)
def test_rover_invalid_instruction(instruction):
    """Test giving an invalid instruction to the rover."""
    rover = Rover(
        x=0,
        y=0,
        compass_bearing='E',
        max_x=3,
        max_y=3,
        name='test'
    )
    with pytest.raises(ValueError):
        rover.process_instruction(instruction)


@pytest.mark.parametrize(
    "x,y,bearing,instructions,final_x,final_y,final_dir",
    [
        (1, 2, 'N', "LMLMLMLMM", 1, 3, 'N'),
        (3, 3, 'E', "MMRMMRMRRM", 5, 1, 'E'),
    ]
)
def test_rover_instructions_list(
    x,
    y,
    bearing,
    instructions,
    final_x,
    final_y,
    final_dir
):
    """Test the instructions list from the sample input file."""
    rover = Rover(
        x=x,
        y=y,
        compass_bearing=bearing,
        max_x=5,
        max_y=5,
        name='test'
    )
    rover.process_instruction_list(instructions)
    assert rover.x == final_x
    assert rover.y == final_y
    assert BEARING_TO_COMPASS_DIRECTION[rover.bearing] == final_dir

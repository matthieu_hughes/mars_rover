"""Tests for PointWithBearing class."""

import pytest

from mars_rover.point_with_bearing import PointWithBearing


@pytest.mark.parametrize(
    "bearing",
    [-540, -180, 180, 540, 900]
)
def test_pwb_initial_bearing(bearing):
    """Test the initial bearing is modulo 360."""
    pwb = PointWithBearing(x=0, y=0, bearing=bearing)
    assert pwb.bearing == 180


@pytest.mark.parametrize(
    "initial_bearing,rotation,final_bearing",
    [
        (0, 0, 0),
        (0, -45, 315),
        (90, 90, 180),
        (180, -60, 120),
        (0, 360, 0),
        (90, 540, 270),
        (90, -450, 0),
        (45.3, 0.8, 46.1),
    ]
)
def test_pwb_rotate(initial_bearing, rotation, final_bearing):
    """Test the rotate() function."""
    pwb = PointWithBearing(x=0, y=0, bearing=initial_bearing)
    pwb.rotate(rotation)
    assert pytest.approx(pwb.bearing, final_bearing)


@pytest.mark.parametrize(
    "initial_x,initial_y,translation,final_x,final_y",
    [
        (0, 0, 1, 1, 0),
        (5, 1, 1, 6, 1),
        (5, 1, -1, 4, 1),
        (5, 0, 3, 8, 0),
        (5, 0, 1.2, 6.2, 0),
        (5, 0, -2.5, 2.5, 0),
    ]
)
def test_pwb_translate_x_direction(
    initial_x,
    initial_y,
    translation,
    final_x,
    final_y
):
    pwb = PointWithBearing(x=initial_x, y=initial_y, bearing=0)
    pwb.translate(distance=translation)
    assert pwb.x == pytest.approx(final_x)
    assert pwb.y == pytest.approx(final_y)


@pytest.mark.parametrize(
    "initial_x,initial_y,translation,final_x,final_y",
    [
        (0, 0, 1, 0, 1),
        (2, 3, 1, 2, 4),
        (2, 3, -2, 2, 1),
        (2, 4, 2.3, 2, 6.3),
        (2, 4, -2.5, 2, 1.5),
    ]
)
def test_pwb_translate_y_direction(
    initial_x,
    initial_y,
    translation,
    final_x,
    final_y
):
    pwb = PointWithBearing(x=initial_x, y=initial_y, bearing=90)
    pwb.translate(distance=translation)
    assert pwb.x == pytest.approx(final_x)
    assert pwb.y == pytest.approx(final_y)

"""Tests for parser functions."""

import pytest

from mars_rover import parsers


@pytest.mark.parametrize(
    "plateau_input",
    [
        '',
        "abc:5 5",
        "Plateau",
        "Plateau:",
        "Plateau:5",
        "Plateau:5,5",
        "Plateau: 5 5",
        "5 5",
        ":5 5",
        "Plateau:5.5 5",
        "Plateau:5 5.2",
        "Plateau:-5 5",
        "Plateau:5 -5",
    ]
)
def test_parse_plateau_invalid(plateau_input):
    """Test invalid plateau configuration string."""
    with pytest.raises(ValueError):
        parsers.parse_plateau(plateau_input)


def test_parse_plateau_valid():
    """Test valid plateau configuration string."""
    x, y = parsers.parse_plateau("Plateau:1 2")
    assert x == 1
    assert y == 2


@pytest.mark.parametrize(
    "landing_input",
    [
        '',
        "abc:1 2 N",
        "Rover Landing",
        "Landing",
        "Rover Landing:",
        "Landing:",
        "Landing:1 2 N",
        "1 2 N",
        ":1 2 N",
        "Rover landing:1 2 N",
        "Rover Landing:1 2",
        "Rover Landing:-1 2 N",
        "Rover Landing:1 -2 N",
        "Rover Landing:1.5 2 N",
        "Rover Landing:1 2.3 N",
        "Rover Landing:1 2 NE",
        "Rover Landing: 1 2 N",
        "Rover Land:1 2 N",
    ]
)
def test_parse_rover_landing_invalid(landing_input):
    """Test invalid rover landing string."""
    with pytest.raises(ValueError):
        parsers.parse_rover_landing(landing_input)


def test_parse_rover_landing_valid():
    """Test valid rover landing string."""
    name, x, y, bearing = parsers.parse_rover_landing("rover Landing:1 2 N")
    assert name == "rover"
    assert x == 1
    assert y == 2
    assert bearing == 'N'


@pytest.mark.parametrize(
    "instructions_input",
    [
        '',
        "abc:MRLM",
        "Rover Instructions",
        "Instructions",
        "Instructions:",
        "Instructions:MRLM",
        "MRLM",
        ":MRLM",
        "Rover Instruct:MRLM",
    ]
)
def test_parse_rover_instructions_invalid(instructions_input):
    """Test invalid rover instructions string."""
    with pytest.raises(ValueError):
        parsers.parse_rover_instructions(instructions_input)


@pytest.mark.parametrize(
    "instructions_input,instructions_output",
    [
        ("Rover Instructions:", ''),
        ("Rover Instructions:MRLM", "MRLM"),
        ("Rover Instructions: MRLM", "MRLM"),
        ("Rover Instructions:MA", "MA"),
    ]
)
def test_parse_rover_instructions_valid(
    instructions_input,
    instructions_output
):
    """Test valid rover instructions string.

    The parser doesn't check if all the instructions are valid - that's up to
    the rover object. So there is a wider range of "valid" instruction strings.
    """
    name, instructions = parsers.parse_rover_instructions(instructions_input)
    assert name == "Rover"
    assert instructions == instructions_output

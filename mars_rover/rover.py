"""Rover class definition."""

from typing import Sequence

from .point_with_bearing import PointWithBearing


BEARING_TO_COMPASS_DIRECTION = {
    0: 'E',
    90: 'N',
    180: 'W',
    270: 'S',
}


COMPASS_DIRECTION_TO_BEARING = {
    'E': 0,
    'N': 90,
    'W': 180,
    'S': 270,
}


class Rover(PointWithBearing):
    """Represents a Mars Rover on an x,y grid with a bearing.

    Extends PointWithBearing in order to:
        * Only allow integer x and y values (rover stays on a grid).
        * Enforce min and max x and y values that the rover cannot exceed.
        * Only allow bearings along the 4 compass directions.
    """

    def __init__(
        self,
        x: int,
        y: int,
        compass_bearing: str,
        max_x: int,
        max_y: int,
        name: str
    ):
        """Initialization.

        Args:
            x (int): The initial x position.
            y (int): The initial y position.
            compass_bearing (str): The initial bearing, as a single letter
                denoting a compass direction (e.g. 'N').
            max_x (int): The edge of the grid in the x direction
            max_y (int): The edge of the grid in hte y direction
            name (str): The name of the rover

        Raises:
            ValueError: If the bearing is not one of the 4 compass directions,
                or the initial x,y position is off the grid.
        """
        self.max_x = max_x
        self.max_y = max_y
        self.name = name

        # Validation of initial position and bearing
        if not (0 <= x <= max_x and 0 <= y <= max_y):
            raise ValueError("Initial position cannot be off the grid.")

        try:
            bearing = COMPASS_DIRECTION_TO_BEARING[compass_bearing]
        except KeyError as e:
            raise ValueError(
                f"Bearing must be one of {list(COMPASS_DIRECTION_TO_BEARING)}."
            ) from e

        super().__init__(x, y, bearing)

    @property
    def is_at_edge_of_grid(self):
        """Determine whether the rover is at the edge of the grid.
        I.e. it would go off the grid if given another "move" instruction.

        Returns:
            bool: Whether the rover, on its current bearing,
                is at the edge of the grid.
        """
        return (
            (self.x == 0 and self.bearing == 180) or
            (self.x == self.max_x and self.bearing == 0) or
            (self.y == 0 and self.bearing == 270) or
            (self.y == self.max_y and self.bearing == 90)
        )

    def process_instruction_list(
        self,
        instruction_list: Sequence,
        disallowed_points: Sequence = None
    ):
        """Process a sequence of instructions from NASA ground control.

        Args:
            instruction_list (Sequence): The list of instructions to process.
                See :func:`process_instruction` for allowed instruction codes.
            disallowed_points (Sequence): The list of disallowed points that
                other rovers currently occupy. In the form of a sequence of
                (x, y) tuples.
        """
        for i in instruction_list:
            self.process_instruction(i, disallowed_points)

    def process_instruction(
            self,
            instruction: str,
            disallowed_points: Sequence = None
    ):
        """
        Process a single instruction from NASA ground control.

        Args:
            instruction (str): An instruction code. Must be one of:
                "L" (rotate left 90*),
                "R" (rotate right 90*), or
                "M" (move 1 point along current bearing).
            disallowed_points (Sequence): The list of disallowed points that
                other rovers currently occupy. In the form of a sequence of
                (x, y) tuples.

        Raises:
            ValueError: If the instruction is not valid.
        """
        instructions_to_methods = {
            "M": self.move,
            "L": self.rotate_left,
            "R": self.rotate_right,
        }
        try:
            instructions_to_methods[instruction](disallowed_points)
        except KeyError as e:
            raise ValueError(
                f"Instruction must be one of {list(instructions_to_methods)}."
            ) from e

    def move(self, disallowed_points=None):
        """Move the rover one unit of distance along its current bearing.

        Raises:
            ValueError: If the instruction would send the rover off the edge of
                the grid
            disallowed_points (Sequence): The list of disallowed points that
                other rovers currently occupy. In the form of a sequence of
                (x, y) tuples.
        """
        if not self.is_at_edge_of_grid:
            self.translate(distance=1)
            if disallowed_points is not None:
                for point in disallowed_points:
                    if self.x == point[0] and self.y == point[1]:
                        # collision
                        raise ValueError(
                            "Rover cannot collide with another rover."
                        )

            # Round to the nearest integer to stay aligned with grid points.
            self.x = int(round(self.x))
            self.y = int(round(self.y))
        else:
            raise ValueError("Cannot move rover off the edge of the grid.")

    def rotate_right(self, *args):
        """Rotate the rover 90 degrees to the right (clockwise)."""
        self.rotate(degrees=-90)

    def rotate_left(self, *args):
        """Rotate the rover 90 degrees to the left (counter-clockwise)."""
        self.rotate(degrees=90)

    def __str__(self):
        """Represent the rover's current state as string.

        Returns:
            str: string describing the rover's name, position, and bearing.
        """
        compass_dir = BEARING_TO_COMPASS_DIRECTION[self.bearing]
        return f"{self.name}:{self.x} {self.y} {compass_dir}"

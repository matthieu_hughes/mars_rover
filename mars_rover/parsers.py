"""Functions for parsing the input file."""


def parse_plateau(plateau_str: str):
    """Parse the plateau information from the input file.

    Only supports non-negative integer values.

    Args:
        plateau_str (str): String with plateau information from the input file.

    Raises:
        ValueError: If the x or y size is not a positive integer.

    Returns:
        tuple (int, int): x and y coordinates denoting the size of the plateau.
    """
    plateau_str = plateau_str.strip()
    _, coordinates = plateau_str.split("Plateau:")
    x, y = coordinates.split(' ')
    x = int(x)
    y = int(y)
    if x < 0 or y < 0:
        raise ValueError("Grid size cannot be negative.")

    return x, y


def parse_rover_landing(landing_str: str):
    """Parse the rover landing information from the input file.

    Only supports non-negative integer values for x and y positions.

    Args:
        landing_str (str): String with landing info from the input file.

    Raises:
        ValueError: If the x or y position is not a positive integer, or
            bearing is not one of 'N', 'S', 'E', or 'W'.

    Returns:
        tuple (str, int, int, str): tuple describing the initial rover
            name, x position, y position and compass direction.
    """
    landing_str = landing_str.strip()
    name, coordinates = landing_str.split(" Landing:")
    x, y, bearing = coordinates.split(' ')
    x = int(x)
    y = int(y)
    if x < 0 or y < 0:
        raise ValueError("Landing coordinates cannot be negative.")

    if bearing not in ('N', 'S', 'E', 'W'):
        raise ValueError("Bearing must be one of N, S, E, or W.")

    return name.strip(), x, y, bearing


def parse_rover_instructions(instructions_str: str):
    """Parse the rover instructions from the input file.

    Args:
        instructions_str (str): String with movement instructions info from
            the input file.

    Returns:
        tuple (str, str): The rover name and movement instructions.
    """
    instructions_str = instructions_str.strip()
    name, instructions = instructions_str.split(" Instructions:")
    return name.strip(), instructions.strip()

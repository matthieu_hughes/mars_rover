"""Tests for main script."""

import argparse
import io
import pytest
from unittest import mock

from main import main as main_script


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(inputfile='_dne.txt'))
def test_inputfile_does_not_exist(mock_args):
    """Test error raised when input file not found."""
    with pytest.raises(FileNotFoundError):
        main_script()


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(inputfile=''))
def test_no_plateau_information(mock_args):
    """Test error is raised if plateau information is not on the first line."""
    file_contents = io.StringIO(
        "Rover1 Landing:0 0 N\nRover1 Instructions:RMM"
    )
    with mock.patch('builtins.open', return_value=file_contents):
        with pytest.raises(ValueError):
            main_script()


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(inputfile=''))
def test_rover_instructions_before_landing(mock_args):
    """Test error is raised if rover instructions are before landing info."""
    file_contents = io.StringIO(
        "Plateau:5 5\nRover1 Instructions:RMM\nRover1 Landing:0 0 N"
    )
    with mock.patch('builtins.open', return_value=file_contents):
        with pytest.raises(ValueError):
            main_script()


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(inputfile=''))
def test_rover_stops_at_grid_edge(mock_args, capsys):
    """Test that rover stops at the edge of the grid."""
    file_contents = io.StringIO(
        "Plateau:5 5\nRover1 Landing:4 4 E\nRover1 Instructions:MMM"
    )
    with mock.patch('builtins.open', return_value=file_contents):
        main_script()
        out, _ = capsys.readouterr()
        assert "Cannot move rover off the edge of the grid" in out
        assert "Rover1:5 4 E" in out


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(inputfile=''))
def test_rover_stops_at_invalid_instruction(mock_args, capsys):
    """Test that rover stops when it encounters an invalid instruction."""
    file_contents = io.StringIO(
        "Plateau:5 5\nRover1 Landing:1 1 E\nRover1 Instructions:MRMA"
    )
    with mock.patch('builtins.open', return_value=file_contents):
        main_script()
        out, _ = capsys.readouterr()
        assert "Instruction must be one of" in out
        assert "Rover1:2 0 S"


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(inputfile=''))
def test_rover_no_instructions(mock_args, capsys):
    """Test a rover stays put if given no instructions."""
    file_contents = io.StringIO("Plateau:5 5\nRover1 Landing:1 2 W")
    with mock.patch('builtins.open', return_value=file_contents):
        main_script()
        out, _ = capsys.readouterr()
        assert "Rover1:1 2 W" in out


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(inputfile=''))
def test_rover_multiple_instructions(mock_args, capsys):
    """Test a rover can handle multiple instruction sequences."""
    file_contents = io.StringIO(
        ("Plateau:5 5\n"
         "Rover1 Landing:1 1 N\n"
         "Rover1 Instructions:MMR\n"
         "Rover1 Instructions:MMR")
    )
    with mock.patch('builtins.open', return_value=file_contents):
        main_script()
        out, _ = capsys.readouterr()
        assert "Rover1:3 3 S" in out


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(inputfile='tests/test_input.txt'))
def test_sample_input_file(mock_args, capsys):
    """Test results from the sample input file."""
    main_script()
    out, _ = capsys.readouterr()
    assert out == "Rover1:1 3 N\nRover2:5 1 E\n"


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(inputfile=''))
def test_rover_collides(mock_args, capsys):
    """Test an error is raised when a rover collides with another rover."""
    # Should collide at 1 2 when rover 2 moves.
    file_contents = io.StringIO(
        ("Plateau:5 5\n"
         "Rover1 Landing:1 1 N\n"
         "Rover1 Instructions:M\n"
         "Rover2 Landing:2 2 W\n"
         "Rover2 Instructions:M")
    )
    with mock.patch('builtins.open', return_value=file_contents):
        main_script()
        out, _ = capsys.readouterr()
        assert 'Rover cannot collide with another rover.' in out

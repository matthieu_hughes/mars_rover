# Mars Rover
The Mars Rover program is a command-line application that initializes one or more rovers on a grid and instructs them to move around, printing out the final position and bearing of the rover(s). The rover(s) follow movement instructions from the input file until they either (1) run out of instructions; (2) encounter an instruction that would move them off the edge of the grid; or (3) encounter an invalid instruction.

## Requirements
The program is designed to run with Python3 on a unix-like command line. To install package requirements run:

`pip3 install -r requirements.txt`

## Usage
The `main.py` file runs the script: `python3 main.py` or `./main.py`.

```
usage: main.py [-h] --inputfile INPUTFILE

Land and move a fleet of Mars Rovers.

optional arguments:
  -h, --help            show this help message and exit
  --inputfile INPUTFILE
                        Path to input configuration file.
```

### Input file
The input file must follow this format:

```
Plateau:5 5
Rover1 Landing:1 2 N
Rover1 Instructions:LMLMLMLMM
Rover2 Landing:3 3 E
Rover2 Instructions:MMRMMRMRRM
```

- The first line must start with the word "Plateau" followed by a colon. The two numbers separated by a space denote the x and y grid sizes respectively.
- Rover landing information is of the form on line 2. It starts with the rover name followed by the word "Landing" and a colon. The values following this denote initial x and y values on the grid then the initial compass direction (N, S, E, or W) the rover is facing.
- Rover instructions are of the form on line 3. It starts with the rover name (matching landing information) followed by the word "Instructions" and a colon. The letters after this are a sequence of instructions to execute. These can be:
    - `M`: Move one grid point along the rover's current direction
    - `R`: Rotate the rover 90 degrees to the right (clockwise)
    - `L`: Rotate the rover 90 degrees to the left (counter-clockwise)
- The input file can contain an arbitrary number of mars rovers with unique names, as long as the "Landing" line for the rover comes before the "Instructions" line. A given rover can also process more than one line of instructions.

## Testing
Run unit tests from the top-level directory of the project with:

`python3 -m pytest tests/`

or

`./test.sh tests/`

NOTE: There is currently an issue with python `PATH`, so running `py.test` directly may not work.

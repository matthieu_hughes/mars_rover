"""PointWithBearing class definition."""

import math


class PointWithBearing:
    """Represents an arbitrary point in an x,y coordinate system with a bearing.

    Angles are taken from the positive x-axis, with
    counter-clockwise as the positive angle of rotation.
    """

    def __init__(self, x: float, y: float, bearing: float):
        """Initialization.

        Args:
            x (float): The initial x position.
            y (float: The initial y position.
            bearing (float): The initial bearing, in degrees measured
                counter-clockwise from the positive x axis (East).

        Raises:
            ValueError: If the bearing is not between 0 and 360.
        """
        self.x = x
        self.y = y
        self.bearing = bearing % 360

    def rotate(self, degrees: float = 90):
        """
        Rotate the point by the given number of degrees.

        The counter-clockwise direction is assigned as the positive direction
        of rotation.

        Args:
            degrees (int): The rotation amount, in degrees.
        """
        self.bearing = (self.bearing + degrees) % 360

    def translate(self, distance: float = 1):
        """
        Translate the point a designated distance along the current bearing.

        Args:
            distance (int): Number of distance units to move.
        """
        self.x += distance * math.cos(math.radians(self.bearing))
        self.y += distance * math.sin(math.radians(self.bearing))

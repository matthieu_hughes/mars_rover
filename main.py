#!/usr/bin/env python3

"""Main entry point providing command-line interface."""

import argparse

from mars_rover import parsers
from mars_rover.rover import Rover


def parse_args():
    """Parse command line arguments.

    Returns:
        argparse.Namespace: Namespace with parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description="Land and move a fleet of Mars Rovers."
    )
    parser.add_argument(
        '--inputfile',
        required=True,
        help="Path to input configuration file."
    )
    args = parser.parse_args()
    return args


def main():
    """Main script.

    Parses command line arguments, parses input file, initializes rovers,
    and processes movement instructions.
    """
    args = parse_args()

    rovers = {}

    with open(args.inputfile, "r") as f:
        # Parse plateau information from the first line.
        plateau_line = next(f)
        try:
            max_x, max_y = parsers.parse_plateau(plateau_line)
        except Exception:
            raise ValueError(
                f"Error parsing plateau information: {plateau_line}"
            )

        # The rest is rover landing info and instructions.
        for line in f:
            if "Landing" in line:
                # Parsing and validation.
                try:
                    name, x, y, bearing = parsers.parse_rover_landing(line)
                except Exception:
                    raise ValueError(("Error parsing rover landing "
                                      f"information: {line}"))

                # Initialize the rover.
                rovers[name] = Rover(x, y, bearing, max_x, max_y, name)

            elif "Instructions" in line:
                # Parsing and validation
                try:
                    name, instructions = parsers.parse_rover_instructions(line)
                except Exception:
                    raise ValueError(
                        f"Error parsing rover instructions: {line}"
                    )
                if name not in rovers:
                    raise ValueError("Rover {name} not landed yet: {line}")

                # Executing instructions.
                try:
                    disallowed_points = []
                    for rover_name in rovers:
                        if name == rover_name:
                            continue
                        rover = rovers[rover_name]
                        disallowed_points.append((rover.x, rover.y))

                    rovers[name].process_instruction_list(
                        instructions, disallowed_points
                    )
                except ValueError as e:
                    # Print out error message if a particular rover has issues
                    # with instructions, but don't stop script execution.
                    message = name + " error: " + str(e)
                    message += " Awaiting further instructions."
                    print(message)

    for r in rovers.values():
        print(str(r))


if __name__ == "__main__":
    main()
